# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.new(
  :login => "admin",
  :email => "jeanjacques@noxs.net",
  :firstname => "Jean-Jacques",
  :lastname => "MOIROUX",
  :phone => "06 25 31 09 33",
  :password => "admin",
  :password_confirmation => "admin",
  :admin => true,
  :forem_admin => true
)
user.save

User.create(
  login: "noxsnono",
  email: "jeanjacquesmoiroux@hotmail.com",
  firstname: "Jean-Jacques",
  lastname: "MOIROUX",
  phone: "06 25 31 09 33",
  password: "server",
  password_confirmation: "server",
  admin: true,
  :forem_admin => true
) unless User.where(email: "jeanjacquesmoiroux@hotmail.com").exists?

User.create(
  login: "test1",
  email: "test1@noxs.net",
  firstname: "first-test1",
  lastname: "last-test1",
  phone: "09xx",
  password: "test1",
  password_confirmation: "test1",
  admin: false
) unless User.where(email: "test1@noxs.net").exists?

User.create(
  login: "moube",
  email: "moube@noxs.net",
  firstname: "first-moube",
  lastname: "last-moube",
  phone: "09yyxxyyxx",
  password: "moube",
  password_confirmation: "moube",
  admin: false
) unless User.where(email: "moube@noxs.net").exists?

group = Group.new( :name => "Student" ) unless Group.where(name: "Student").exists?

group = Group.new( :name => "Noob" ) unless Group.where(name: "Noob").exists?

group = Group.new( :name => "Admin" ) unless Group.where(name: "Admin").exists?

group = Group.new( :name => "Reader" ) unless Group.where(name: "Reader").exists?

ticket = Ticket.create(
  user_id: 3,
  user_login: "test1",
  admin_assign_id: 1,
  admin_assign_login: "admin",
  title: "test1 asign admin",
  message: "test1 asign admin test1 asign admin",
)
ticket.save

ticket = Ticket.create(
  user_id: 1,
  user_login: "admin",
  admin_assign_id: 1,
  admin_assign_login: "admin",
  title: "admin asign admin",
  message: "admin asign admin admin asign admin"
)
ticket.save

ticket = Ticket.create(
  user_id: 3,
  user_login: "test1",
  admin_assign_id: 1,
  admin_assign_login: "admin",
  title: "test1 again asign admin",
  message: "test1 asign admin again test1 asign admin"
)
ticket.save

ticket = Ticket.create(
  user_id: 4,
  user_login: "moube",
  admin_assign_id: 1,
  admin_assign_login: "admin",
  title: "moube again asign admin",
  message: "moube asign admin again moube asign admin"
)
ticket.save