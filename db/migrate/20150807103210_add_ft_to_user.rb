class AddFtToUser < ActiveRecord::Migration
  def change
    add_column :users, :locale, :string, default: ""
    add_column :users, :login42, :string
    add_column :users, :years42, :string
    add_column :users, :month42, :string
    add_column :users, :location42, :string
    add_column :users, :email42, :string
    add_column :users, :homedirectory42, :string
    add_column :users, :data_photo, :text
  end
end
