class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.string :user_login
      t.integer :admin_assign_id
      t.string :admin_assign_login
      t.string :title, default: "No Title"
      t.text :message, default: "Empty message"
      t.boolean :open, default: true

      t.timestamps null: false
    end
  end
end
