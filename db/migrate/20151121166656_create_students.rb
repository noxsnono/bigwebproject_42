class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :uidNumber
      t.string :uid
      t.string :cn
      t.string :givenName
      t.string :sn
      t.string :year_42
      t.string :month_42
      t.string :location_42
      t.text :data_photo

      t.timestamps null: false
    end
  end
end
