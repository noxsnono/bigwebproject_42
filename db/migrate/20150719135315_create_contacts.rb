class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :firstname, default: ""
      t.string :lastname, default: ""
      t.string :entity, default: ""
      t.string :phone, default: ""
      t.string :email, default: ""
      t.string :title, default: ""
      t.text :message, default: ""

      t.timestamps null: false
    end
  end
end
