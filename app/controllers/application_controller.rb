class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :logg

  before_filter :set_locale

  helper_method :forem_user

  def bind_ldap_user(login, passwd)
    ldap = Net::LDAP.new(:host => 'ldap-proxy.42.fr', :port => 1636, :encryption => :simple_tls)
    ldap.auth "jmoiroux", jmoiroux_passwd

    result = ldap.bind_as(:base => 'dc=42,dc=fr', :filter => "(uid=#{login})", :password => passwd)
  end

  def logg
    if user_signed_in?
      log = Log.new(user_id: current_user.id, controller: controller_name, action: action_name, url: request.fullpath)
      log.save
    end
  end

  def forem_user
    current_user
  end

  def authenticate_admin!
    if user_signed_in? && current_user.admin
      true
    else
      redirect_to new_user_session_path
    end
  end

  def change_locale
    if current_user && I18n.available_locales.include?(params[:locale])
      current_user.locale = params[:locale]
      current_user.save
    end
  end
  private
  def set_locale
    I18n.available_locales = [:en, :fr]
    if user_signed_in? && current_user.locale != ""
      newLocal = current_user.locale
    elsif params[:locale].present?
      newLocal = params[:locale]
    else
      newLocal = I18n.default_locale
    end
    I18n.locale = newLocal
    Rails.application.routes.default_url_options[:locale] = I18n.locale
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit( :login, :firstname, :lastname, :phone, :email, :password, :password_confirmation, :remember_me) }
    # devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :login42, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login42, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:login, :firstname, :lastname, :phone,:email, :password, :password_confirmation, :current_password) }
  end

  protected
  def jmoiroux_passwd
    "unknow"
  end

end
