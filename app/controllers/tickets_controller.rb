class TicketsController < ApplicationController
  before_action :authenticate_user!, only: [:destroy, :show, :edit, :update, :new, :user_index]

  before_action :authenticate_admin!, only: [:index, :show_admin, :edit_admin, :update_admin]

  before_action :logg, only: [ :user_index, :show, :create, :update, :index, :show_admin, :update_admin, :destroy]

  ### User section
  def user_index
    @tickets = Ticket.where(user_id: current_user.id)
  end

  def show
    @ticket = Ticket.find(params[:id])
  end

  def new
  end

  def create
    @ticket = Ticket.new(params_create_by_user)

    if @ticket.save
      redirect_to main_app.user_tickets_path
    else
      render 'new'
    end
  end

  def edit
    @ticket = Ticket.find(params[:id])
  end

  def update
    @ticket = Ticket.find(params[:id])

    if @ticket.update(params_update_by_user)
      redirect_to "/" + I18n.locale.to_s + "/tickets/" + @ticket.id.to_s
    else
      render 'edit'
    end
  end

  ### Admin section
  def index
    @tickets = Ticket.all
  end

  def show_admin
    @ticket = Ticket.find(params[:id])
  end

  def edit_admin
    @ticket = Ticket.find(params[:id])
    @admins = User.where(admin: true)
  end

  def update_admin
    @ticket = Ticket.find(params[:id])

    if @ticket.update(params_update_by_admin)
      @adminAssign = User.find(@ticket.admin_assign_id)
      @ticket.admin_assign_login = @adminAssign.login
      @ticket.save
      redirect_to "/" + I18n.locale.to_s + "/adm/ticket/" + @ticket.id.to_s
    else
      render 'edit_admin'
    end
  end

  def destroy
    @ticket = Ticket.find(params[:id])

    if authenticate_admin!
      @ticket.destroy
      @path = tickets_path
    elsif current_user && current_user.id == @ticket.user_id
      @ticket.destroy
      @path = user_tickets_path
    else
      @path = root_path
    end

    redirect_to @path
  end

### Params Permits
  private
    def params_create_by_user
      params.require(:ticket).permit(:title, :message).merge(user_id: current_user.id, user_login: current_user.login)
    end

  private
    def params_update_by_user
      params.require(:ticket).permit(:title, :message, :open)
    end

  private
    def params_update_by_admin
      params.require(:ticket).permit(:title, :message, :admin_assign_id, :admin_assign_login, :open)
    end
end