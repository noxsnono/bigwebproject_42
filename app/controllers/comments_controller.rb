class CommentsController < ApplicationController
  before_action :logg, only: [:create]
  def create
    @ticket = Ticket.find(params[:ticket_id])
    @comment = Comment.new(comment_params)
    @comment = @ticket.comments.create(comment_params)

    redirect_to tickets_path
  end

  def comment_params
    params.require(:comment).permit(:commenter, :body).merge(user_id: current_user.id, login: current_user.login)
  end
end