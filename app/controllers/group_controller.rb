class GroupController < ApplicationController
  before_action :authenticate_user!
  before_action :logg, only: [:index]

  def index
    @groups = Group.all
  end
end
