class LogController < ApplicationController
  before_action :authenticate_user!
  before_action :logg, only: [:index, :destroy]

  def index
    @logs = Log.all.limit(20).order('created_at DESC')
  end

  def destroy
  end
end
