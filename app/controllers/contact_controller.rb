class ContactController < ApplicationController
  before_action :logg, only: [:show, :create, :index, :destroy]
  def index
    @contacts = Contact.all
  end

  def new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to root_path
    else
      @contact_error = "Create Failed"
    end
  end

  def show
  end

  def contact_params
    params.require(:contact).permit(:firstname, :lastname, :entity, :phone, :email, :title, :message)
  end
end
