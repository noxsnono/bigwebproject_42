class StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :logg, only: [:index, :filter]

  def index
    ap "index"
    ldap = Net::LDAP.new(
      :host => 'ldap.42.fr',
      :port => 636,
      :encryption => :simple_tls,
      :method => :simple
    )

    @entries = ldap.search(
      :base => 'ou=august,ou=2013,ou=paris,ou=people,dc=42,dc=fr',
      :filter => Net::LDAP::Filter.eq( "uid", "*" ),
      :attributes => [ 'uidNumber', 'uid', 'cn', 'givenName', 'sn']
    )

    if params.has_key?(:uidNumber)
      @entries = @entries.sort_by { |entry| entry.uidNumber } if params[:uidNumber] == "ASC"
      @entries = @entries.sort_by { |entry| entry.uidNumber }.reverse if params[:uidNumber] == "DESC"
    elsif params.has_key?(:uid)
      @entries = @entries.sort_by { |entry| entry.uid } if params[:uid] == "ASC"
      @entries = @entries.sort_by { |entry| entry.uid }.reverse if params[:uid] == "DESC"
    elsif params.has_key?(:cn)
      @entries = @entries.sort_by { |entry| entry.cn } if params[:cn] == "ASC"
      @entries = @entries.sort_by { |entry| entry.cn }.reverse if params[:cn] == "DESC"
    elsif params.has_key?(:givenName)
      @entries = @entries.sort_by { |entry| entry.givenName } if params[:givenName] == "ASC"
      @entries = @entries.sort_by { |entry| entry.givenName }.reverse if params[:givenName] == "DESC"
    elsif params.has_key?(:sn)
      @entries = @entries.sort_by { |entry| entry.sn } if params[:sn] == "ASC"
      @entries = @entries.sort_by { |entry| entry.sn }.reverse if params[:sn] == "DESC"
    end

    @student = Student.new

  end

  def filter

    ap "Filter"
    ldap = Net::LDAP.new(
      :host => 'ldap.42.fr',
      :port => 636,
      :encryption => :simple_tls,
      :method => :simple
    )
    f_params = filter_params

    ap params
    if f_params.has_key?(:login)
      fil_login = Net::LDAP::Filter.eq( "uid", "#{f_params[:login].to_s}*" )
    end
    if f_params.has_key?(:cn)
      fil_cn = Net::LDAP::Filter.eq( "cn", "#{f_params[:cn].to_s}*" )
    end
    if f_params.has_key?(:uidnumber)
      fil_uidnumber = Net::LDAP::Filter.eq( "uid", "#{f_params[:uidnumber].to_s}*" )
    end

    if fil_login && fil_cn && fil_uidnumber
      fil_1 = Net::LDAP::Filter.join(fil_login, fil_cn)
      fil_2 = Net::LDAP::Filter.join(fil_1, fil_uidnumber)
      fil_1 = fil_2
    elsif fil_login && fil_cn
      fil_1 = Net::LDAP::Filter.join(fil_login, fil_cn)
    elsif fil_login && fil_uidnumber
      fil_1 = Net::LDAP::Filter.join(fil_login, fil_uidnumber)
    elsif fil_cn && fil_uidnumber
      fil_1 = Net::LDAP::Filter.join(fil_cn, fil_uidnumber)
    end

    if fil_1
      ftth = fil_1
    elsif fil_login
      ap "Filter 1"
      ftth = fil_login
    elsif fil_cn
      ftth = fil_cn
    elsif fil_uidnumber
      ftth = fil_uidnumber
    else
      ftth = Net::LDAP::Filter.eq( "uid", "*" )
    end

    @entries = ldap.search(
      :base => 'ou=august,ou=2013,ou=paris,ou=people,dc=42,dc=fr',
      :filter => ftth,
      :attributes => [ 'uidNumber', 'uid', 'cn', 'givenName', 'sn']
    )
    if f_params
      @student = Student.new(
        login: f_params[:login],
        cn: f_params[:cn],
        uidNumber: f_params[:uidnumber]
      )
      ap @student
    else
      @student = Student.new
    end

  end

  private
  def filter_params
    params.require(:student).permit(:login, :cn, :uidnumber)
  end

end
