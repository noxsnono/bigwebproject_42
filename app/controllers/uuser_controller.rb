class UuserController < ApplicationController
  before_action :authenticate_admin!

  before_action :logg, only: [:index, :show]

  def index
    @users = User.all
  end

  def show
  end

  def become_user
    return unless authenticate_admin!
    sign_in(:user, User.find(params[:id]), { :bypass => true })
    redirect_to root_url
  end
end
