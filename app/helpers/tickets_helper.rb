module TicketsHelper
  def resource_name
    :ticket
  end

  def resource_class
    Ticket
  end

  def resource
    @ticket ||= Ticket.new
  end

  def comment_resource_name
    :comment
  end

  def comment_resource_class
    Comment
  end

  def comment_resource
    @comment ||= Comment.new
  end

end
