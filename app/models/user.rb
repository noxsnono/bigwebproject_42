class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => {email: false, login42: true}

  validates :login, presence: true, uniqueness: true

   attr_accessor :login42

  # hack for remember_token
  def authenticatable_token
    Digest::SHA1.hexdigest(email)[0,29]
  end

  # Front-end navbar administration button
  def admin?
    if self && self.admin
      true
    else
      false
    end
  end

  def current_admin
    self && self.admin == true
  end

  def current_admin_forum
    self && self.forem_admin == true
  end

  # Forem
  def forem_name
    self.login
  end

  # Forem email gravatar
  def forem_email
    self.email
  end

end
