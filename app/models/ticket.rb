class Ticket < ActiveRecord::Base
  has_many :comments
  validates :title, presence: true
  validates :message, presence: true

  ### User
  def self.user_ticket_open_all(userId)
    self.where(user_id: userId, open: true)
  end

  def self.user_ticket_close_all(userId)
    self.where(user_id: userId, open: false)
  end

  ### Admin
  def self.ticket_open_all
    self.where(open: true)
  end

  def self.ticket_close_all
    self.where(open: false)
  end

  def self.ticket_no_assign
    self.where(open: true, admin_assign_id: nil)
  end

  def self.ticket_assign_to_me(userId)
    self.where(admin_assign_id: userId, open: true)
  end
end