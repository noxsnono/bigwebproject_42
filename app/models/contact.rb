class Contact < ActiveRecord::Base
  validates_format_of :email, :with => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  validates_presence_of :title
  validates_presence_of :message
end
