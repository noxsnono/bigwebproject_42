docker run -d \
-p 80:80 \
-p 443:443 \
-p 1337:1337 \
-p 3000:3000 \
-p 3306:3306 \
-p 5432:5432 \
-p 6379:6379 \
-p 7379:7379 \
-p 8080:8080 \
-p 8081:8081 \
-p 8082:8082 \
-p 8083:8083 \
-P \
-h="DOCK-RAILS-f0" \
-v /media/root/framework-2:/var/www/framework-2 \
noxsnono/framework0:v000004

boot2docker ip && docker port $(docker ps -lq)
