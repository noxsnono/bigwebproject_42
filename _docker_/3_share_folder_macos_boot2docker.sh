echo "\nCurrent directory:\t" $PWD
boot2docker stop
VBoxManage sharedfolder add boot2docker-vm --automount --name "framework-2" --hostpath $PWD \
&& echo "\tSharing framework-2 to VBox SUCCESS"
boot2docker up
echo 'sudo mkdir -p /media/root/framework-2' | boot2docker ssh
echo 'sudo chmod 777 /media/root/framework-2' | boot2docker ssh
echo 'sudo mount -t vboxsf framework-2 /media/root/framework-2' | boot2docker ssh
echo "\tSharing VBox framework-2 to Docker SUCCESS"