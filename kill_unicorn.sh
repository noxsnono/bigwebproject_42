#!/bin/sh
unicorn_pid=`cat log/pids/unicorn.pid`
echo "Kill Unicorn ($unicorn_pid)"
kill $unicorn_pid