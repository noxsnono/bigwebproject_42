chmod -R 777 *
service nginx start
service postgresql start
cd /var/www/framework-2 && bundle install
cd /var/www/framework-2 && rake db:drop ; rake db:create && rake db:migrate && rake db:seed
cd /var/www/framework-2 && ./start_unicorn_8080.sh
