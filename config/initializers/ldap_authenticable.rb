require 'net/ldap'
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class LdapAuthenticatable < Authenticatable
      def authenticate!
        if params[:user]
          ldap = Net::LDAP.new(
            :host => 'ldap.42.fr',
            :port => 636,
            :encryption => :simple_tls
          )
          ldap.auth login42, password
          @result = ldap.bind_as(
            :base => 'dc=42,dc=fr',
            :filter => "(uid=#{login42})",
            :password => password
          )
          if @result
            user = User.find_by(email: "#{login42}@student.42.fr")
            if user
              success!(user)
            else
              pepper = nil
              cost = 10
              encrypt_new = ::BCrypt::Password.create("#{password}#{pepper}", :cost => cost).to_s

              if login42 == "jmoiroux"
                user = User.create!(
                  login: login42,
                  email: "#{login42}@student.42.fr",
                  firstname: @result[0].givenname.to_s.sub!('["', "").sub!('"]', ""),
                  lastname: @result[0].sn.to_s.sub!('["', "").sub!('"]', ""),
                  login42: @result[0].uid.to_s.sub!('["', "").sub!('"]', ""),
                  homedirectory42: @result[0].homedirectory.to_s.sub!('["', "").sub!('"]', ""),
                  email42: "#{login42}@student.42.fr",
                  data_photo: @result[0].jpegPhoto.to_s.sub!('["', "").sub!('"]', ""),
                  admin: true,
                  password: encrypt_new
                )
              else
                user = User.create!(
                  login: login42,
                  email: "#{login42}@student.42.fr",
                  firstname: @result[0].givenname.to_s.sub!('["', "").sub!('"]', ""),
                  lastname: @result[0].sn.to_s.sub!('["', "").sub!('"]', ""),
                  login42: @result[0].uid.to_s.sub!('["', "").sub!('"]', ""),
                  homedirectory42: @result[0].homedirectory.to_s.sub!('["', "").sub!('"]', ""),
                  email42: "#{login42}@student.42.fr",
                  data_photo: @result[0].jpegPhoto.to_s.sub!('["', "").sub!('"]', ""),
                  password: encrypt_new
                )
              end

              success!(user)
            end
          else
            fail(:invalid_login)
          end
        end
      end

      def email
        params[:user][:email]
      end

      def login42
        params[:user][:login42]
      end

      def password
        params[:user][:password]
      end

    end
  end
end

Warden::Strategies.add(:ldap_authenticatable, Devise::Strategies::LdapAuthenticatable)
