Rails.application.routes.draw do

  scope '/:locale' do
    mount Forem::Engine, :at => '/forums'

    devise_for :users

    get 'students'                    => 'students#index'
    post 'students/filter'                   => 'students#filter', as: 'filter_student'

    get 'group/index'                       => 'group#index'
    get 'users_list'                        => 'uuser#index'
    get 'users_list/:id'                    => 'uuser#show'
    get 'users_list/:id/logas'              => 'uuser#become_user', as: "admin_logas_user"
    get 'users_list/change_locale/:locale'  => 'uuser#set_local', as: "change_locale_user"

    get 'welcomes'                          => 'welcomes#index'
    get 'sujet/framework0'                  => 'welcomes#framework0'
    get 'sujet/framework1'                  => 'welcomes#framework1'
    get 'sujet/framework2'                  => 'welcomes#framework2'
    get 'sujet/bigwebproject'               => 'welcomes#bigwebproject'

    get 'contact'                           => 'contact#index'
    get 'contact/new'                       => 'contact#new'
    post 'contact/create'                   => 'contact#create'
    get 'contact/:id'                       => 'contact#show'

    get 'log/index'

    resources :tickets do
      resources :comments
    end

    get '/user/tickets'                     => 'tickets#user_index'

    scope '/adm' do
      get '/ticket/:id'                     => 'tickets#show_admin', as: "adm_ticket_show"
      get '/ticket/:id/edit'                => 'tickets#edit_admin', as: "adm_ticket_edit"
      patch '/ticket/:id/edit'              => 'tickets#update_admin'
    end

  end

  ActiveAdmin.routes(self)
  root :to => 'welcomes#index'

end
