#!/bin/sh
unicorn_pid=`cat log/pids/unicorn.pid`
echo "Restarting Unicorn ($unicorn_pid)"
kill $unicorn_pid
sleep 1
./start_unicorn_8080.sh
