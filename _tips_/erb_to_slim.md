Dans le gem file en dev group si vous voulez
    gem 'html2haml'
    gem 'haml2slim'

apres a la racine de l'application faire la commande qui suit
    find ./app/views -name '*.erb' | xargs -I file sh -c 'html2haml --erb file | haml2slim > $(echo file | sed 's/erb/slim/') && rm file'
